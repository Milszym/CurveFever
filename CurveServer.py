import socket
import threading
from docx import Document
import time

TCP_IP = '192.168.5.125'
TCP_PORT = 1234
BUFFER_SIZE = 1024
CLIENTS=[]
MESSAGE_AFTER_CONNECTION = 'Klienci przyjeci'


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(2)

conn=[]
addr=[]

for i in range(0,4):
    connTmp,addrTmp = s.accept()
    conn.append(connTmp)
    addr.append(addrTmp)
    print(addr[i])

for i in range(0,4):
    potwierdzenie=str(i+1)
    conn[i].send(potwierdzenie.encode())


def toClient(conn, conn2, conn3, conn4, kto):
    while True:
        data = conn.recv(BUFFER_SIZE)
        if not data:
            break

        if kto==1:
            if data.decode()=='l':
                conn2.send(b'1 skrecil w lewo')
                conn3.send(b'1 skrecil w lewo')
                conn4.send(b'1 skrecil w lewo')
                print("1 skrecil w lewo")
            elif data.decode()=='p':
                conn2.send(b'1 skrecil w prawo')
                conn3.send(b'1 skrecil w prawo')
                conn4.send(b'1 skrecil w prawo')
                print("1 skrecil w prawo")

        if kto==2:
            if data.decode()=='l':
                conn2.send(b'2 skrecil w lewo')
                conn3.send(b'2 skrecil w lewo')
                conn4.send(b'2 skrecil w lewo')
                print("2 skrecil w lewo")
            elif data.decode()=='p':
                conn2.send(b'2 skrecil w prawo')
                conn3.send(b'2 skrecil w prawo')
                conn4.send(b'2 skrecil w prawo')
                print("2 skrecil w prawo")

        if kto==3:
            if data.decode()=='l':
                conn3.send(b'3 skrecil w lewo')
                conn2.send(b'3 skrecil w lewo')
                conn4.send(b'3 skrecil w lewo')
                print("3 skrecil w lewo")
            elif data.decode()=='p':
                conn3.send(b'3 skrecil w prawo')
                conn2.send(b'3 skrecil w prawo')
                conn4.send(b'3 skrecil w prawo')
                print("3 skrecil w prawo")

        if kto == 4:
            if data.decode() == 'l':
                conn3.send(b'4 skrecil w lewo')
                conn2.send(b'4 skrecil w lewo')
                conn4.send(b'4 skrecil w lewo')
                print("4 skrecil w lewo")
            elif data.decode() == 'p':
                conn3.send(b'4 skrecil w prawo')
                conn2.send(b'4 skrecil w prawo')
                conn4.send(b'4 skrecil w prawo')
                print("4 skrecil w prawo")
        #conn.send(data)


threading.Thread(target=toClient, args=(conn[0], conn[1], conn[2], conn[3], 1)).start()
threading.Thread(target=toClient, args=(conn[1], conn[0], conn[2], conn[3], 2)).start()
threading.Thread(target=toClient, args=(conn[2], conn[0], conn[1], conn[3], 3)).start()
threading.Thread(target=toClient, args=(conn[3], conn[0], conn[1], conn[2], 4)).start()

